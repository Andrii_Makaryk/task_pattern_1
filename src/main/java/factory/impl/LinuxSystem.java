package factory.impl;

import factory.Systems;

public class LinuxSystem implements Systems {

    public void provision() {
        System.out.println("Linux provision");
    }

    public void update() {

    }

    public void restart() {

    }
}
