package factory;

import factory.impl.*;

public class SystemFactory {
    public static Systems createSystem(TypeSystem type){
        Systems system=null;
        if(type==TypeSystem.WIN ){
            system=new WindowsSystem();
        }else if(type==TypeSystem.LIN){
            system=new LinuxSystem();
        }else if(type==TypeSystem.MAC){
            system=new MacosSystem();
        }else if(type==TypeSystem.SOL){
            system=new SolarisSystem();
        }else if(type==TypeSystem.UNIX){
            system=new UnixSystem();
        }
        return system;
    }
}
