package factory;

public interface Systems {

    void provision();

    void update();

    void restart();
}
