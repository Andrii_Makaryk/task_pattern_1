package factory;

public class Client {
    public static void main(String[] args) {
        Systems systemLin=SystemFactory.createSystem(TypeSystem.LIN);
        systemLin.provision();
    }
}
